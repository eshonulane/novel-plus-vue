# my-demo

[演示地址](http://138.128.217.21:23951/)
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 后端
后端: [译羽/小说精品屋-plus ](https://gitee.com/eshonulane/novel-plus)

Fork来源: [小说精品屋/小说精品屋-plus](https://gitee.com/novel_dev_team/novel-plus)