import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import Message from './assets/js/elementui-message.js';
import 'element-ui/lib/theme-chalk/index.css';
import axios from './assets/js/axios.js';
import global from './assets/js/global.js';
import util from './assets/js/util.js';
import * as filters from './assets/js/filters.js';
import VueCookies from 'vue-cookies';
import vAvoidRepeat from './assets/js/avoid-repeat.js'

import VueSwiper from 'vue-awesome-swiper';
import 'swiper/swiper-bundle.css';
import Swiper2, { Navigation, Pagination, EffectFade, Autoplay } from 'swiper'
Swiper2.use([Navigation, Pagination, EffectFade, Autoplay])

Vue.use(vAvoidRepeat);
Vue.use(VueSwiper);
Vue.use(ElementUI);
Vue.prototype.$message = Message;

Vue.config.productionTip = false
Vue.use(axios);
Vue.use(VueCookies);
Vue.prototype.global = global;
Vue.prototype.util = util;

for(let key in filters){
  Vue.filter(key,filters[key])//插入过滤器名和对应方法
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

console.log('version:'+process.env.VUE_APP_version);