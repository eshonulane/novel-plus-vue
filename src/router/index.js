import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/Index.vue'
import global from '../assets/js/global.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },{
    path: '/',
    redirect: '/index/home'
  }
]

const mainRoute = {
  path: '/index', 
  component: Index, 
  name: 'index',
  redirect: '/index/home',
  children: [
    {path: 'home', component: () => import('../views/Home.vue'), meta:{title:'首页-小说精品屋'}},
    {path: 'login', component: () => import('../views/user/Login.vue')},
    {path: 'register', component: () => import('../views/user/Login.vue')},
    {path: 'registerCodes', component: () => import('../views/user/registerCodes.vue')},
    {path: 'allBook', component: () => import('../views/book/BookList.vue'), meta:{title:'全部作品-小说精品屋'}},
    {path: 'swiperTest', component: () => import('../views/book/SwiperTest.vue')},
    {path: 'slideTest', component: () => import('../views/book/SlideTest.vue')},
    // {path: 'allBookIndex', component: () => import('../views/book/BookIndexList.vue')},
    {path: 'allBookIndex', component: () => import('../views/InComing.vue')},
    {path: 'ranked', component: () => import('../views/book/BookRank.vue')},
    {path: 'recharge', component: () => import('../views/user/Recharge.vue')},
    {path: 'rechargeForm', component: () => import('../views/user/RechargeForm.vue')},
    {path: 'author', component: () => import('../views/InComing.vue')},
    {path: 'bookDetail', component: () => import('../views/book/BookDetail.vue')},
    {path: 'bookCommentlist', component: () => import('../views/book/BookCommentList.vue')},
    {
      path: 'user', 
      component: () => import('../views/user/NavUser.vue'),
      redirect: 'user/userInfo',
      children:[
        {path:'userInfo', component: () => import('../views/user/UserInfo.vue')},
        {path:'favorites', component: () => import('../views/user/Favorites.vue')},
        {path:'comment', component: () => import('../views/user/Comment.vue')},
        {path:'feedback', component: () => import('../views/user/Feedback.vue')},
        {path:'feedbackNew', component: () => import('../views/user/FeedbackNew.vue')},
        {path:'setup', component: () => import('../views/user/Setup.vue')},
        {path:'setupName', component: () => import('../views/user/SetupName.vue')},
        {path:'setupGender', component: () => import('../views/user/SetupGender.vue')},
        {path:'setupPasswd', component: () => import('../views/user/SetupPasswd.vue')},
        {path:'blacklist', component: () => import('../views/user/Blacklist.vue')},
      ]
    },
    {path: 'newsDetail', component: () => import('../views/system/NewsDetail.vue')},
    {path: 'about', component: () => import('../views/InComing.vue')}
  ]
}
// const mainRoute = {
//   path: '/home', 
//   component: Home, 
//   name: 'home',
//   children: [
//     {path: 'InComing', component: () => import('../views/InComing.vue'), name:'InComing'}
//   ]
// }

const router = new VueRouter({
  routes: [...routes, mainRoute],
})
router.beforeEach((to, from, next) => {
  // console.log(to);
  // console.log(from);
  // console.log(document.referrer);
  /* 路由发生变化修改页面title */
  // console.log(global);
  // console.log(to.meta);
  if (to.meta.title) {
    document.title = to.meta.title
  }else if(document.title != global.metaTitle){
    document.title = global.metaTitle
  }
  //需要登录的页面在前端先判断是否登录
  

  next();
})
// router.afterEach((to, from) => {
//   console.log(history);
//   console.log(history.state);
// })

export default router
