
export {convertWan, dateToyMdHms, hideMobile}

import moment from 'moment'

function convertWan(value){
  return (value / 10000).toFixed(2);
}

function dateToyMdHms(value){
  return moment(value).format('YYYY-MM-DD HH:mm:ss');
}

function hideMobile(value){
  if(!value){
    return "";
  }
  if(value.length == 1){
    return "*";
  }
  let hideNum = parseInt(value.length/2);
  return value.replace(/./g, "*").substr(0, hideNum) + value.substr(hideNum);
}

