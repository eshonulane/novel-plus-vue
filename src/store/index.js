import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loginStatus:0,
    searchKey:'',
    leftTuochUid:'',
    state:null
  },
  mutations: {
    refreshLoginStatus(state){
      state.loginStatus++;
    },
    searchKeyExcute(state, serachValue){
      state.searchKey = serachValue;
    }
  },
  actions: {
  },
  modules: {
  }
})
