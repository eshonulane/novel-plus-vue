
const global = require('./src/assets/js/global.js');
// console.log(global.metaTitle);
module.exports = {
    devServer:{
        port : 8000,
        open : false,
        // proxy: {
        //     '/api':{
        //         target: 'http://localhost:8080',
        //         changeOrigin: true,  //是否跨域
        //         pathRewrite: {
        //         '^/api': ''   //重写接口
        //         }
        //     }
        // }
    },
    css:{
        loaderOptions: {
            sass: {
                additionalData: `@import "@/style/_style.scss";`,
            }
        }
    },
    chainWebpack: config => {
        config.plugin('html').tap(args => {
            args[0].title = global.metaTitle
            return args
        })
    }
}